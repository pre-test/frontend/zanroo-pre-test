import React, { useState, useEffect } from 'react'
import scss from '~/public/static/scss/table.scss'
import swal from "sweetalert";

export default (props) => {
  const [edit, setEdit] = useState([])
  const [headerTable] = useState([
    { name: 'Name' },
    { name: 'Age' },
    { name: 'Nickname' },
    { name: 'Action' }
  ])
  const [dataTableList, setDataTableList] = useState([])
  // Logical function
  const onEnter = (event, callback) => event.key === 'Enter' && callback()
  const editDataInRow = (action) => {
    const editAction = (value) => {
      let newOpenInput = [...edit]
      newOpenInput[action.index].openInput = value
      setEdit(newOpenInput)
    }
    if (action.type === 'edit') {
      if (edit[action.index].openInput) {
        // เช็คข้อมูลครบไหม
        if (dataTableList[action.index].name && dataTableList[action.index].nickName && dataTableList[action.index].age) {
          // เช็คชื่อจริงซ้ำ
          const checkAlreadyName = dataTableList.findIndex(e => e.name === dataTableList[action.index].name
              && e.id !== dataTableList[action.index].id)
          if (checkAlreadyName < 0) {
            localStorage.setItem('userList', JSON.stringify(dataTableList));
            editAction(false)
            swal("สำเร็จ", "แก้ไขข้อมูลผู้ใช้สำเร็จ", "success");
          } else {
            swal("ไม่สำเร็จ", "มีข้อมูลผู้ใช้ท่านนี้แล้ว", "error");
          }
        } else {
          swal("ข้อมูลไม่ครบถ้วน", "กรุณากรอกข้อมูลผู้ใช้ให้ครบถ้วน", "warning");
        }
      } else {
        editAction(true)
      }
    } else {
      const localData = JSON.parse(localStorage.getItem('userList'))
      setDataTableList(Object.assign([], localData))
      editAction(false)
    }
  }
  const deleteDataInRow = (action) => {
    swal("ยืนยันการลบ", `ต้องการลบข้อมูลของ "${action.name}" หรือไม่?`, "warning", {
      buttons: {
        cancel: "ยกเลิก",
        catch: {
          text: "ยืนยัน",
          value: "confirm",
        }
      },
    }).then((value) => {
      if (value === 'confirm') {
        const index = dataTableList.findIndex(e => e.id === action.id)
        // dataTableList
        dataTableList.splice(index, 1)
        setDataTableList(Object.assign([], dataTableList))
        // edit data flag
        edit.splice(action.index, 1)
        setEdit(Object.assign([], edit))
        localStorage.setItem('userList', JSON.stringify(dataTableList));
        swal("สำเร็จ", `ลบข้อมูลของ "${action.name}" สำเร็จ`, "success")
      }
    });
  }

  // Get userList from localDb when user added.
  useEffect(() => {
    setDataTableList(props.userList)
    // set Edit
    let editList = []
    props.userList.map((value) => {
      editList.push({ openInput: false, rowId: value.id })
      setEdit(editList)
    })
  }, [props.userList])

  return (
    <div id={scss.userManagementTable}>
      <div id={scss.table} className="table-box">
        <div id={scss.customTableScroll} className="table-scroll">
          <table id={scss.customTable}>
            <thead id={scss.customHeader} className="header-table">
              <tr className="row-header-box">
                {headerTable.map((columnHeader, id) =>
                  <th className="column-header-box" key={id} scope="col">
                    <div className="topic-column">{columnHeader.name}</div>
                  </th>
                )}
              </tr>
            </thead>
            <tbody className="body-table">
              { dataTableList.length === 0 ?
                <tr>
                  <td className="center-text" colSpan="4"> <span> ไม่มีรายการ </span> </td>
                </tr> :
                dataTableList.map((rowDetail, rowIndex) =>
                  <tr key={rowIndex}>
                    <td className="left-text">
                      { !edit[rowIndex].openInput ?
                        <span>{rowDetail.name}</span> :
                        <input type="text" id="inputName" className="input-name" placeholder="Enter name"
                               value={dataTableList[rowIndex].name}
                               onChange={e => { let newName = [...dataTableList]
                                 newName[rowIndex].name = e.target.value
                                 setDataTableList(newName) }
                               }
                               onKeyPress={e => onEnter(e,() => editDataInRow({ type: 'edit', id: rowDetail.id, index: rowIndex }))} />
                      }
                    </td>
                    <td>
                      { !edit[rowIndex].openInput ?
                        <span>{rowDetail.age}</span> :
                        <input type="text" id="inputAge" className="input-age" placeholder="Age"
                               value={dataTableList[rowIndex].age}
                               onChange={e => { let newName = [...dataTableList]
                                 newName[rowIndex].age = e.target.value.replace(/\D/,'')
                                 setDataTableList(newName)}
                               }
                               onKeyPress={e => onEnter(e,() => editDataInRow({ type: 'edit', id: rowDetail.id, index: rowIndex }))} />
                      }
                    </td>
                    <td className="left-text">
                      { !edit[rowIndex].openInput ?
                        <span>{rowDetail.nickName}</span> :
                        <input type="text" id="inputNickname" className="input-nickname" placeholder="Enter Nickname"
                               value={dataTableList[rowIndex].nickName}
                               onChange={e => { let newName = [...dataTableList]
                                 newName[rowIndex].nickName = e.target.value
                                 setDataTableList(newName)}
                               }
                               onKeyPress={e => onEnter(e,() => editDataInRow({ type: 'edit', id: rowDetail.id, index: rowIndex }))} />
                      }
                    </td>
                    <td id={scss.buttonBox}>
                      <template className="row">
                        <div className="col-md-6 p-0 pt-2 pb-2">
                          <button id="editButton" className="edit-button"
                                  onClick={() => editDataInRow({ type: 'edit', id: rowDetail.id, index: rowIndex })}>Edit</button>
                        </div>
                        <div className="col-md-6 p-0 pt-2 pb-2">
                          { !edit[rowIndex].openInput ?
                              <button id="deleteButton" className="delete-button"
                                      onClick={() => deleteDataInRow({ type: 'delete', id: rowDetail.id, name: rowDetail.name, index: rowIndex })}>Delete</button> :
                              <button id="cancelButton" className="cancel-button"
                                      onClick={() => editDataInRow({ type: 'cancel', id: rowDetail.id, index: rowIndex })}>Cancel</button>
                          }
                        </div>
                      </template>
                    </td>
                  </tr>
                )
              }
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
