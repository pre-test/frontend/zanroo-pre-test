import React, { useEffect, useState } from "react";
import Table from '~/components/managementTable'
import swal from 'sweetalert';
import scss from '~/public/static/scss/userManagement.scss'
export default (props) => {
  const [showInputForm, setShowInputForm] = useState(false)
  const [addUser, setAddUser] = useState(false)
  const [userList, setUserList] = useState([])
  const [localDbData, setLocalDbData] = useState([])
  const initialState = {
    id: 1,
    name: '',
    nickName: '',
    age: ''
  }
  const [formData, setFormData] = useState(initialState)
  // logical function
  const onEnter = (event, callback) => event.key === 'Enter' && callback()
  const clearForm = () => {
    setShowInputForm(false)
    setFormData(initialState)
  }
  const handleFormAdd = (status) => {
    if (status === 'save') {
      // เช็คข้อมูลครบไหม
      if (formData.name && formData.nickName && formData.age) {
        // เช็คชื่อจริงซ้ำ
        const checkAlreadyName = localDbData.findIndex(e => e.name === formData.name)
        if (checkAlreadyName < 0) {
          const localParseData = JSON.parse(localStorage.getItem('userList'));
          localParseData.length > 0 ? formData.id = localParseData[localParseData.length - 1].id + 1 : '';
          setUserList(userList => [...localParseData, Object.assign({}, formData)]);
          setAddUser(true)
          swal("สำเร็จ", "เพิ่มข้อมูลผู้ใช้สำเร็จ", "success");
        } else {
          swal("ไม่สำเร็จ", "มีข้อมูลผู้ใช้ท่านนี้แล้ว", "error");
        }
      } else {
        swal("ข้อมูลไม่ครบถ้วน", "กรุณากรอกข้อมูลผู้ใช้ให้ครบถ้วน", "warning");
      }
    } else {
      clearForm();
    }
  }
  // Set data to localDb
  useEffect(() => {
    const checkLocalDb = JSON.parse(localStorage.getItem('userList'))
    !checkLocalDb ? localStorage.setItem('userList', JSON.stringify(userList)) : setLocalDbData(JSON.parse(localStorage.getItem('userList')))
    if (addUser) {
      localStorage.setItem('userList', JSON.stringify(userList));
      setLocalDbData(userList);
    }
    clearForm()
  }, [userList])

  return (
    <section id={scss.userManagement}>
      <div id={scss.bodyBox} className="container">
        <Table userList={localDbData} />
        { !showInputForm ? (
            <div className="add-user">
              <button id="addUserButton" className="add-user-button"
                      onClick={() => { setShowInputForm(true) }}>Add</button>
            </div>
          ) : (
            <div className="form-input" >
              <input type="text" id="inputName" className="input-name" placeholder="Enter name"
                     value={formData.name}
                     onChange={e => setFormData( {...formData, name: e.target.value})}
                     onKeyPress={e => onEnter(e,() => handleFormAdd('save'))} />
              <input type="text" id="inputAge" className="input-age" placeholder="Age"
                     value={formData.age}
                     onChange={e => setFormData({...formData, age: e.target.value.replace(/\D/,'')})}
                     onKeyPress={e => onEnter(e,() => handleFormAdd('save'))} />
              <input type="text" id="inputNickname" className="input-nickname" placeholder="Enter Nickname"
                     value={formData.nickName}
                     onChange={e => setFormData( {...formData, nickName: e.target.value})}
                     onKeyPress={e => onEnter(e,() => handleFormAdd('save'))} />
              <button id="saveButton" className="save-button"
                      onClick={() => handleFormAdd('save')}>Save</button>
              <button id="cancelButton" className="cancel-button"
                      onClick={() => handleFormAdd()}>Cancel</button>
            </div>
          )
        }
      </div>
    </section>
  );
}
